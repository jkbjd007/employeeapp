import 'package:flutter/cupertino.dart';

Color appColor = Color(0xff00B251);
Color drawerColor = Color(0xff27373D);
Color drawerSelectMenuColor = Color(0xff3D5860);
Color disableColor = Color(0xffdddddd);
Color whiteColor = Color(0xffffffff);
Color errorColor = Color(0xffff0000);
Color backGroundColor = Color(0xffEDEDED);
Color darkColor = Color(0xff033E71);
Color AppBarStartColor =  Color(0xff134389);
Color AppBarEndColor = Color(0xff4EAF48);