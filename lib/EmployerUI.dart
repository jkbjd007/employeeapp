import 'package:demo/Services/EmployeService.dart';
import 'package:demo/Services/Provider.dart';
import 'package:demo/common_widgets/backIcon.dart';
import 'package:demo/common_widgets/circularImage.dart';
import 'package:demo/common_widgets/custom_appbar.dart';
import 'package:demo/common_widgets/loader.dart';
import 'package:demo/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:demo/styles/size.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class EmployerUI extends HookWidget {

  final GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    EmployeServices employeServices = useProvider(EmployeServiceProvider);
    useEffect(
          () {
            employeServices.FetchEmployes();
        Future.microtask(() async {

        });

        return () {
          // Dispose Objects here
        };
      },
      const [],
    );
    return Scaffold(
        key: _scaffoldkey,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(size.convert(context, 80)),
          child: CustomAppBar(
            color1: Colors.white,
            color2: Colors.white,
            hight: size.convert(context, 80),
            parentContext: context,
            leadingIcon: Row(
              children: [
                Icon(FontAwesomeIcons.arrowLeft,color: Colors.grey,),
                SizedBox(width: size.convertWidth(context, 10),),
                Container(
                  child: Text("Back",
                    style: style.PoppinsSemiBold(
                        color: Colors.black45,
                        fontSize: size.convert(context, 14)
                    ),),
                )
              ],
            ),
            trailingIcon: Row(
              children: [
                Icon(FontAwesomeIcons.search,color: Colors.grey,),
                SizedBox(width: size.convertWidth(context, 10),),
                Icon(FontAwesomeIcons.ellipsisV,color: Colors.grey,)
              ],
            ),
          ),
        ),
        body: _body(context,employeServices: employeServices));
  }

  _body(context,{EmployeServices employeServices}) {
    return employeServices.loadingEmployees ? loader() : Container(
      margin: EdgeInsets.symmetric(
          vertical: size.convert(context, 10)
      ),
      child: ListView.separated(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemBuilder: (context, index) {
            return Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convertWidth(context, 25)),
              padding: EdgeInsets.symmetric(
                  vertical: size.convert(context, 10),
                  horizontal: size.convertWidth(context, 10)
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(size.convert(context, 15)),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Row(
                      children: [
                        circularCenterImage(
                          imageUrl: "assets/icons/ProfileImage.png",
                          assetImage: true,
                          w: size.convert(context, 25),
                          h: size.convert(context, 25),
                        ),
                        SizedBox(width: size.convertWidth(context, 10),),
                        Container(
                          child: Column(
                            // mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: Text("${employeServices?.em[index]?.name}",
                                  style: style.PoppinsSemiBold(
                                    fontSize: size.convert(context, 16),
                                  ),),
                              ),
                              SizedBox(
                                height: size.convert(context, 5),
                              ),
                              Container(
                                child: Text("${employeServices?.em[index]?.title}",
                                  style: style.PoppinsRegular(
                                      fontSize: size.convert(context, 12),
                                      color: Colors.black
                                  ),),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: [
                        Icon(FontAwesomeIcons.phoneAlt,
                          color: Color(0xff6d4fce),
                          size: size.convert(context, 16),),
                        SizedBox(width: size.convertWidth(context, 20),),
                        Icon(FontAwesomeIcons.solidComments,
                          color: Color(0xff6d4fce),)
                      ],
                    ),
                  )
                ],
              ),
            );
          },
          separatorBuilder: (context, index) {
            return Container(height: size.convert(context, 10),);
          },
          itemCount: employeServices?.em?.length?? 0),
    );
  }

}

