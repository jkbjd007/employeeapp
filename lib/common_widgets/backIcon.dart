import 'package:flutter/material.dart';
import 'package:demo/styles/size.dart';
class backIcon extends StatelessWidget {
  Function ontap;
  backIcon({this.ontap});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.pop(context);
      },
      child: Container(
        child: Icon(Icons.keyboard_backspace, color: Colors.white,
          size: size.convert(context, 30),),
      ),
    );
  }
}
