import 'package:demo/Services/EmployeService.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

final EmployeServiceProvider = new ChangeNotifierProvider<EmployeServices>((ref) => new EmployeServices());