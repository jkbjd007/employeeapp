import 'package:demo/Models/EmployeeModel.dart';
import 'package:flutter/cupertino.dart';

class EmployeServices extends ChangeNotifier {
  List<EmployeeModel> em = [];
  bool loadingEmployees  = true;
  FetchEmployes(){
      print("call get employee");
      em.add(EmployeeModel(name: "Saif Uddin",title: "Designer"));
      em.add(EmployeeModel(name: "Alina Lui",title: "Logo Designer"));
      em.add(EmployeeModel(name: "Sabrina khan",title: "Content writer"));
      em.add(EmployeeModel(name: "Mark Havard",title: "Co-founder"));
      em.add(EmployeeModel(name: "Alex Pesenka",title: "User Interface Designer"));
      em.add(EmployeeModel(name: "Saif khan",title: "Head Designer"));
      em.add(EmployeeModel(name: "Karim javic",title: "Developer"));
      em.add(EmployeeModel(name: "Salman Khan",title: "Flutter Developer"));
      em.add(EmployeeModel(name: "javardina javid",title: "UI Designer"));
      em.add(EmployeeModel(name: "jannate joy",title: "Designer"));

      Future.delayed(Duration(seconds: 3),(){
        print("listner call");
        print("Length of employee ${em.length}");
        loadingEmployees = false;
        notifyListeners();
      });
  }
}

